# TV From A to Z

Tv From A to Z is a REST API which provide you some ways to find relevant information about movies, tv series. your favorite artist and more.
This solution was built using technologies such as:

 - Spring Boot
 - Hibernate
 - Spring Data JPA
 - Lombok
 - MySQL
 - jUnit
 - Git
 - Angular (for front-end view)

# A big-picture from the solution

![enter image description here](https://ibin.co/4Go5NhQpcLFp.png)

*Note: default ports for applications are: 4200 (front-end) and 8080 (back-end).*

# Prerequisite

Before executing the back-end project, you need to setup your MySQL database and load all the tables based on IMDb datasets available for downloading at https://datasets.imdbws.com/.

# Executing the solutions

## TV-fromAtoZ (REST API)
This solution is responsible for providing all information required by the code challenge description. For the sake of clarity over the API's endpoints, I provided a documentation using Swagger 2 and you can check this out at ->   [http://localhost:8080/swagger-ui.html](http://localhost:8080/api/swagger-ui.html).

For executing the project, please, run the command below:

    mvn spring-boot:run

I added some unit tests to the solution and you can run them executing the command:

    mvn test


## TV-fromAtoZ-ui (Front-end)
I've created as well a simple application using Angular to attend the requirement #3 which is to find movies and tv series by primary title or original one.

Remember! You need to have node installed in your computer. Need help? [See](https://nodejs.org/en/).

Execute the project typing the command below:

    ng serve

When everything is up, you are going to see this page:
![enter image description here](https://ibin.co/w800/4Gp5rOARWM8W.png)

## Considerations

This code challenge was very interesting and I wish to have more free time enough to finish all the requirements (missing the last one, only).

Working with huge databases as IMDb is always tricky and you have to find ways to retrieve/persist data losing the minimum possible in terms of performance. For example:
retrieve data by a text column using LIKE operator is always a terrible idea whether you are concerned about performance. Instead of using it, I chose to create a FULLTEXT Index for some columns and then, execute queries using FULLTEXT Search.

Regarding the Six degrees of Kevin Bacon challenge, I got the idea and mind a solution, and I would like to share with you even not developing that in fact. The Six degrees of Kevin Bacon is a "algorithm" which posits that any two people on Earth are six or fewer acquaintance links apart.

The solution basically consists in store information (title, casting) into a Graph structure because this kind of structure fits very well the case when you have things which connect to each other. Moreover, Graph has node and edges which can be defined like:

 - Node: stores the artist name (actor/actress);
 - Edge: the title which connects them;

Considering a Graph filled with all data available, we could use the Breadth-First Search method to find the degree as expected. But, why BFS approach? I believe BFS is better because, different from DFS, it explores all nodes available by level, I mean, it explores all nodes one step away, the two steps away and so on.

See image below:
![enter image description here](https://ibin.co/w800/4GorwjLKQiEf.jpg)

Finally, applying BFS for searching, when you find Kevin Bacon, the level you are is the degree expected. As a plus, you can show the whole connection between them to understand how the algorithm came to the answer.

 
