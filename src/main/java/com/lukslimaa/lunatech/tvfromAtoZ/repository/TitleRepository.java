package com.lukslimaa.lunatech.tvfromAtoZ.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lukslimaa.lunatech.tvfromAtoZ.model.Title;

@Repository
public interface TitleRepository extends JpaRepository<Title, Long> {
	
	List<Title> findByGenresContainingOrderByRatings_AverageRatingDesc(String genres);
	
}
