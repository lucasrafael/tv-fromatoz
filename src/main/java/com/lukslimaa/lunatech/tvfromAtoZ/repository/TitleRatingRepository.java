package com.lukslimaa.lunatech.tvfromAtoZ.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lukslimaa.lunatech.tvfromAtoZ.model.TitleRating;

@Repository
public interface TitleRatingRepository extends JpaRepository<TitleRating, Long> {
	List<TitleRating> findTop25ByOrderByAverageRatingDesc();
}
