package com.lukslimaa.lunatech.tvfromAtoZ.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class ListOfNamesException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ListOfNamesException(String message) {
		super(message);
	}

}
