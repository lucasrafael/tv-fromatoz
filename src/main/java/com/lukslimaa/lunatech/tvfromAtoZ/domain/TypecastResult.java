package com.lukslimaa.lunatech.tvfromAtoZ.domain;

import java.io.Serializable;

import lombok.Data;

public @Data class TypecastResult implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String genres;
	public Integer occurQuantity;
}
