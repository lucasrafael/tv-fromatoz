package com.lukslimaa.lunatech.tvfromAtoZ.domain;

import java.io.Serializable;

import lombok.Data;

public @Data class CastResult implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String name;
	public String category;

}
