package com.lukslimaa.lunatech.tvfromAtoZ.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .apiInfo(metaData())
          .select()      
          .apis(RequestHandlerSelectors.basePackage("com.lukslimaa.lunatech.tvfromAtoZ.controller"))              
          .paths(PathSelectors.any())
          .build();                                           
    }
	
	private ApiInfo metaData(){
		return new ApiInfoBuilder()
				.title("Tv From A to Z API")
				.description("Rest API to support you to find relevant information about movies, series and more.")
				.version("V1.0")
				.contact(contact())
				.license("MIT")
				.build();
	}
	
	private Contact contact(){
		return new springfox.documentation.service.Contact(
				"Lucas Lima", 
				"github.com/lukslimaa", 
				"lucasrafael.uece.@gmail.com");
	}

}
