package com.lukslimaa.lunatech.tvfromAtoZ.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name="title_ratings", indexes={
		@Index(columnList="tconst", name="title_code")
})
public @Data class TitleRating implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
	
	@NotBlank
	@Digits(integer=2, fraction=1)
	@Column(name="average_rating")
	private BigDecimal averageRating;
	
	@NotBlank
	private Integer numVotes;
	
	@OneToOne
	@JoinColumn(name="tconst")
	@JsonIgnore
	private Title title;

}
