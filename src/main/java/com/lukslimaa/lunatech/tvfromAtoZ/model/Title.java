package com.lukslimaa.lunatech.tvfromAtoZ.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Entity
@Table(name="title_basics", indexes = {
		@Index(columnList="tconst", name="title_code")
})
public @Data class Title implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotBlank
	@ApiModelProperty(notes="Title unique code.")
	private String tconst;
	
	@NotBlank
	@ApiModelProperty(notes="The type/format of the title (e.g. movie, short, tvseries, tvepisode, video, etc)")
	private String titleType;
	
	@NotBlank
	@ApiModelProperty(notes="The more popular title / the title used by the filmmakers on promotional materials at the point of release")
	private String primaryTitle;
	
	@NotBlank
	@ApiModelProperty(notes="Original title, in the original language")
	private String originalTitle;
	
	@NotBlank
	@ApiModelProperty(notes="Non-adult title; 1: adult title")
	private Boolean isAdult;
	
	@NotBlank
	@ApiModelProperty(notes="Represents the release year of a title. In the case of TV Series, it is the series start year")
	private String startYear;
	
	@ApiModelProperty(notes="TV Series end year. \\N for all other title types")
	private String endYear;
	
	@NotBlank
	@ApiModelProperty(notes="Primary runtime of the title, in minutes")
	private Integer runtimeMinute;
	
	@NotBlank
	@ApiModelProperty(notes="Includes up to three genres associated with the title")
	private String genres;
	
	@OneToOne(mappedBy="title", fetch=FetchType.LAZY)
	private TitleRating ratings;
	
	@OneToOne(mappedBy="title", fetch=FetchType.LAZY)
	private Crew crew;
	
	@OneToMany(mappedBy="title", fetch=FetchType.LAZY)
	private List<TitlePrincipal> principal;
}
