package com.lukslimaa.lunatech.tvfromAtoZ.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name="title_principals", indexes= {
		@Index(columnList="tconst, ordering, nconst", name="idx_principal_UNIQUE", unique=true),
		@Index(columnList="nconst", name="idx_nconst")
})
public @Data class TitlePrincipal implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long id;
	
	@NotBlank
	@Column
	private Integer ordering;
	
	@NotBlank
	@Column
	private String nconst;
	
	@Column
	private String category;
	
	@Column
	private String job;
	
	@Column
	private String characters;
	
	@ManyToOne
	@JoinColumn(name="tconst")
	@JsonIgnore
	private Title title;

}
