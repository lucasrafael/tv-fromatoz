package com.lukslimaa.lunatech.tvfromAtoZ.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name="title_crew", indexes= {
		@Index(columnList="tconst", name="idx_tconst")
})
public @Data class Crew implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotBlank
	private String tconst;
	
	@Column
	private String directors;
	
	@Column
	private String writers;
	
	@OneToOne
	@JoinColumn(name="tconst")
	@JsonIgnore
	private Title title;
	

}
