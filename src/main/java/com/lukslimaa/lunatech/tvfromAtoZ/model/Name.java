package com.lukslimaa.lunatech.tvfromAtoZ.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="name_basics", indexes= {
		@Index(columnList="nconst, primaryName", name="nconst_UNIQUE", unique=true)
})
public class Name {
	
	@Id
	@ApiModelProperty(notes="Alphanumeric unique identifier of the name/person")
	private String nconst;
	
	@Column
	@NotBlank
	@ApiModelProperty(notes="Name by which the person is most often credited")
	private String primaryName;
	
	@Transient
	@ApiModelProperty(notes="His/Her role in a movie or tv serie.")
	private String category;
	
	@Column(length=4)
	@NotBlank
	private String birthYear;
	
	@Column(length=4)
	private String deathYear;
	
	@Column
	@ApiModelProperty("The top-3 professions of the person")
	private String primaryProfession;
	
	@Column
	@ApiModelProperty("Titles the person is known for")
	private String knownForTitles;

}
