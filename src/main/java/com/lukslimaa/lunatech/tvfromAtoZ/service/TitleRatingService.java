package com.lukslimaa.lunatech.tvfromAtoZ.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lukslimaa.lunatech.tvfromAtoZ.model.TitleRating;
import com.lukslimaa.lunatech.tvfromAtoZ.repository.TitleRatingRepository;

@Service
public class TitleRatingService {
	
	@Autowired
	TitleRatingRepository repo;
	
	public List<TitleRating> getTop25TitlesByRating(){
		return repo.findTop25ByOrderByAverageRatingDesc();
	}
	
}
