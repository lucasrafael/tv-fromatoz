package com.lukslimaa.lunatech.tvfromAtoZ.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukslimaa.lunatech.tvfromAtoZ.domain.TypecastResult;

@Service
public class TypecastService {
	
	@Autowired
	EntityManager em;

	
	public String wasTypecasted(String name){
		
		Integer maxValue = 0;
		Integer total = 0;
		String jsonResult = "";
		
		try {
			
			List<TypecastResult> listOfTitlesWithQuantityByGenres = (List<TypecastResult>) getNumberOfTitlesByGenreFor(name);
			
			for(int i = 0; i < listOfTitlesWithQuantityByGenres.size(); i++){
				if(listOfTitlesWithQuantityByGenres.get(i).getOccurQuantity() > maxValue) {
					maxValue = listOfTitlesWithQuantityByGenres.get(i).getOccurQuantity();
				}
				
				total += listOfTitlesWithQuantityByGenres.get(i).getOccurQuantity();
			}
			
			boolean isTypecast = isTheMaxQuantityHalfOfTotalWorkAtLeast(maxValue, total);
			jsonResult = createJsonResult(name, isTypecast);
		
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} 
		
		return jsonResult;
	}

	private String createJsonResult(String name, boolean isTypecast) throws JsonProcessingException {
		
		Map<String, String> map = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();
		
		map.put("actor/actress", name);
		map.put("typecasting", Boolean.toString(isTypecast));
		
		String jsonResult = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
		return jsonResult;
	}
	
	
	/**
	 * @param maxValue this is the max amount of titles grouped by genre found for a given actor/actress 
	 * @param total	   this is the total amount of titles found for a given actor/actress (for all genres)
	 * @return		   this method return true if the maxValue is greater than or equals to (at least) 50% of the total amount
	 */
	public boolean isTheMaxQuantityHalfOfTotalWorkAtLeast(Integer maxValue, Integer total) {
		if(total == null || total == 0) {
			return false;
		} else {
			BigDecimal percent = new BigDecimal(maxValue * 1.0 / total);
			BigDecimal minPercent = new BigDecimal(0.5);
			int comparison = percent.compareTo(minPercent);
			return (comparison == 0 || comparison == 1) ? true : false;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<TypecastResult> getNumberOfTitlesByGenreFor(String name) {
		
		Query query = em.createNativeQuery(
				"select t.genres			as genres " + 
				", count(t.genres)	as occur_quantity " +
				"from title_principals p " +
				"join name_basics b on b.nconst = p.nconst " +
			    "join title_basics t on t.tconst = p.tconst " +
			    "where match(b.primaryName) against('\""+ name +"\"') " +
			    "group by t.genres");
		
		List<Object[]> result = (List<Object[]>) query.getResultList();
		List<TypecastResult> typecast = mapResultListToTypecastObject(result);
		return typecast;
	}

	
	public List<TypecastResult> mapResultListToTypecastObject(List<Object[]> result) {
		List<TypecastResult> typecast = new ArrayList<TypecastResult>(result.size());
		for(Object[] o : result){
			TypecastResult t = new TypecastResult();
			t.setGenres((String)o[0]);
			t.setOccurQuantity(((BigInteger) o[1]).intValue());
			typecast.add(t);
		}
		return typecast;
	}
	
}
