package com.lukslimaa.lunatech.tvfromAtoZ.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.lukslimaa.lunatech.tvfromAtoZ.domain.CastResult;
import com.lukslimaa.lunatech.tvfromAtoZ.model.Title;
import com.lukslimaa.lunatech.tvfromAtoZ.repository.TitleRepository;

@Service
public class TitleService {
	
	@Autowired
	TitleRepository repo;
	
	@Autowired
	EntityManager em;
	
	@Cacheable("titles")
	public List<Title> getAllTitlesAvailable(String genre){
		return repo.findByGenresContainingOrderByRatings_AverageRatingDesc(genre);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Title> getTitleByName(String name) {
		Query query = em.createNativeQuery(
				"SELECT title.* FROM title_basics title \n"
				+ "where match(title.primary_title, title.original_title) against ('\"" + name + "\"') \n"
				+ "order by title.tconst;", 
				Title.class);
		
		List<Title> listOfTitles = (List<Title>) query.getResultList();
		return listOfTitles;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Title> getAllTitlesSharedBy(List<String> names){
		
		String listOfNames = createStringOfNamesForSearching(names);
		
		Query query = em.createNativeQuery(
				"with titles_shared as (\n" + 
				"\n" + 
				"select p.tconst\n" + 
				"from title_principals as p\n" + 
				"\n" + 
				"	inner join title_basics as title on title.tconst = p.tconst\n" + 
				"    inner join name_basics	as person on person.nconst = p.nconst\n" + 
				"\n" + 
				"where match(person.primaryName) against('"+ listOfNames +"')\n" + 
				"group by p.tconst\n" + 
				"having count(*) = "+names.size()+"\n" + 
				")\n" + 
				"\n" + 
				"select tb.* from titles_shared as ts\n" + 
				"	join title_basics as tb on tb.tconst = ts.tconst\n" + 
				";", Title.class);
		
		List<Title> list = (List<Title>) query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<CastResult> getNamesForTitle(String title) {
		
		Query query = em.createNativeQuery(
				"select n.primaryName, p.category\n" + 
				"from title_principals p \n" + 
				"join name_basics n on n.nconst = p.nconst\n" + 
				"where p.tconst = '" + title + "'\n" + 
				"order by p.category");
		
		List<Object[]> result = (List<Object[]>) query.getResultList();
		List<CastResult> titleCastList = mapResultListToTypecastObject(result);
		return titleCastList;
		
	}
	
	private String createStringOfNamesForSearching(List<String> names) {
		return names.stream()
				.map(s -> "\"" + s + "\"")
				.collect(Collectors.joining(", "));
	}
	
	public List<CastResult> mapResultListToTypecastObject(List<Object[]> result) {
		List<CastResult> cast = new ArrayList<CastResult>(result.size());
		for(Object[] o : result){
			CastResult t = new CastResult();
			t.setName((String) o[0]);
			t.setCategory((String) o[1]);
			cast.add(t);
		}
		return cast;
	}

}
