package com.lukslimaa.lunatech.tvfromAtoZ;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvFromAtoZApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvFromAtoZApplication.class, args);
	}
}
