package com.lukslimaa.lunatech.tvfromAtoZ.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukslimaa.lunatech.tvfromAtoZ.domain.CastResult;
import com.lukslimaa.lunatech.tvfromAtoZ.exception.ListOfNamesException;
import com.lukslimaa.lunatech.tvfromAtoZ.model.Title;
import com.lukslimaa.lunatech.tvfromAtoZ.service.TitleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/title")
@Api(value="tvfromAtoZ", description="Operations pertaining to titles.")
public class TitlesController {
	
	@Autowired
	TitleService service;
	
	/* swagger configuration for the endpoints */
	@ApiResponses(value= {
			@ApiResponse(code = 200, message="Successfully retrieved list"),
			@ApiResponse(code = 401, message="You're not authorized to view this resource"),
			@ApiResponse(code = 404, message="The resource you were trying to reach does not exist")
	})
	
	@ApiOperation(value="Get a list of titles by a given genre sorted by the average rating.", response=Title.class)
	@GetMapping(value="/genre/{name}", produces="application/json")
	public List<Title> getHelloWorld(@PathVariable(value="name") String name){
		return service.getAllTitlesAvailable(name);
	}
	
	@ApiOperation("Application replies with a list of movies or TV shows that both people have shared. You should provide two or more artist names.")
	@GetMapping("/shared")
	public List<Title> getListOfTitlesSharedBy(@RequestParam(value="name", required=true) List<String> names){
		
		if(names.isEmpty())
			throw new ListOfNamesException("You did not give us actor/actress names! Please try again!");
		
		if(names.size() == 1)
			throw new ListOfNamesException("You must provide at least two names for searching!");
			
		
		return service.getAllTitlesSharedBy(names);
	}
	
	@GetMapping(value="/{name}", produces="application/json")
	public List<Title> getTitleByName(@PathVariable(value="name") String name){
		return service.getTitleByName(name);
	}
	
	@ApiOperation("Get all names envolved in a movie or tv serie by the code.")
	@GetMapping(value="/cast/{tconst}", produces="application/json")
	public String getCastNamesForTitle(@PathVariable(value="tconst") String tconst) {
		ObjectMapper mapper = new ObjectMapper();
		String castJson = "";
		try {
			List<CastResult> list = service.getNamesForTitle(tconst);
			castJson = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} 
		return castJson;
	}
	
	
}
