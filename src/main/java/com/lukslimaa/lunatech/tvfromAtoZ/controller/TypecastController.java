package com.lukslimaa.lunatech.tvfromAtoZ.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lukslimaa.lunatech.tvfromAtoZ.domain.TypecastResult;
import com.lukslimaa.lunatech.tvfromAtoZ.service.TypecastService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/typecast")
@Api(value="tvfromAtoZ", description="Operations pertaining to typecasting.")
public class TypecastController {
	
	@Autowired
	TypecastService service;
	
	@ApiResponses(value= {
			@ApiResponse(code = 200, message="Successfully retrieved list"),
			@ApiResponse(code = 401, message="You're not authorized to view this resource"),
			@ApiResponse(code = 404, message="The resource you were trying to reach does not exist")
	})
	
	@ApiOperation(value="Find out whether a actor/actress is typecasting.", response=TypecastResult.class)
	@GetMapping(value="/{name}", produces = "application/json; charset=UTF-8")
	public String wasTypecasted(@PathVariable(value="name") String name){
		return service.wasTypecasted(name);
	}
	

}
