package com.lukslimaa.lunatech.tvfromAtoZ.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lukslimaa.lunatech.tvfromAtoZ.model.TitleRating;
import com.lukslimaa.lunatech.tvfromAtoZ.service.TitleRatingService;

@RestController
@RequestMapping("/rating")
public class TitleRatingController {
	
	@Autowired
	TitleRatingService service;
	
	@GetMapping("/top25")
	public List<TitleRating> getTop25TitlesByRating(){
		return service.getTop25TitlesByRating();
	}

}
