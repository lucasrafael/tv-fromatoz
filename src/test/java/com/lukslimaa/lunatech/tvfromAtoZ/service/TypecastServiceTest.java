package com.lukslimaa.lunatech.tvfromAtoZ.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class TypecastServiceTest {
	
	
	TypecastService service;

	@Before
	public void setUp() throws Exception {
		this.service = new TypecastService();
	}

	
	/* TESTING:> isTheMaxQuantityHalfOfTotalWorkAtLeast -- BEGIN */
	@Test
	public void Should_returnTrue_WhenMaxValueIsMoreThanTotal() {
		
		//definition
		Integer maxValue = 101;
		Integer total = 200;
		
		//action
		boolean result = service.isTheMaxQuantityHalfOfTotalWorkAtLeast(maxValue, total);
		
		//assertion
		assertThat(result, is(true));
		
	}
	
	@Test
	public void Should_returnTrue_WhenMaxValueIsEqualToHalfOfTotalAmount() {
		
		//definition
		Integer maxValue = 100;
		Integer total = 200;
		
		//action
		boolean result = service.isTheMaxQuantityHalfOfTotalWorkAtLeast(maxValue, total);
		
		//assertion
		assertThat(result, is(true));
		
	}
	
	@Test
	public void Should_returnFalse_WhenMaxValueIsLessThanHalfOfTotalAmount() {
		
		//definition
		Integer maxValue = 2;
		Integer total = 200;
		
		//action
		boolean result = service.isTheMaxQuantityHalfOfTotalWorkAtLeast(maxValue, total);
		
		//assertion
		assertThat(result, is(false));
		
	}
	
	@Test
	public void Should_returnFalse_WhenTotalValueIsZERO() {
		
		//definition
		Integer maxValue = 0;
		Integer total = 0;
		
		//action
		boolean result = service.isTheMaxQuantityHalfOfTotalWorkAtLeast(maxValue, total);
		
		//assertion
		assertThat(result, is(false));
		
	}
	
	@Test
	public void Should_returnFalse_WhenValuesAreNULL() {
		
		//action
		boolean result = service.isTheMaxQuantityHalfOfTotalWorkAtLeast(null, null);
		
		//assertion
		assertThat(result, is(false));
		
	}
	/* TESTING:> isTheMaxQuantityHalfOfTotalWorkAtLeast -- END */

}
